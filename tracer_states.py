Tstates = {
'oklahoma' : {'contributions' : ['https://guardian.ok.gov/PublicSite/Docs/BulkDataDownloads/2018_ContributionLoanExtract.csv.zip',
                            'https://guardian.ok.gov/PublicSite/Docs/BulkDataDownloads/2017_ContributionLoanExtract.csv.zip'],
            'disbursements' : ['https://guardian.ok.gov/PublicSite/Docs/BulkDataDownloads/2018_ExpenditureExtract.csv.zip',
                          'https://guardian.ok.gov/PublicSite/Docs/BulkDataDownloads/2017_ExpenditureExtract.csv.zip'],
            'file_name' : 'ok_state_all.xls'
            },

'alabama' : { 'contributions' : ['http://fcpa.alabamavotes.gov/PublicSite/Docs/BulkDataDownloads/2018_CashContributionsExtract.csv.zip',
                                'http://fcpa.alabamavotes.gov/PublicSite/Docs/BulkDataDownloads/2017_CashContributionsExtract.csv.zip'],
             'disbursements' : ['http://fcpa.alabamavotes.gov/PublicSite/Docs/BulkDataDownloads/2018_ExpendituresExtract.csv.zip',
                                'http://fcpa.alabamavotes.gov/PublicSite/Docs/BulkDataDownloads/2017_ExpendituresExtract.csv.zip'],
            'file_name' : 'al_state_all.xls'

            },

'colorado' : {'contributions' : ['http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2018_ContributionData.csv.zip',
                                'http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2017_ContributionData.csv.zip'],
            'disbursements' : ['http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2018_ExpenditureData.csv.zip',
                          'http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2017_ExpenditureData.csv.zip'],
            'file_name' : 'co_state_all.xls'
            }
}
