from FEC_Deploy_Remove_Withdrawals_March import *

def download_loop(committee_ids, dest_file):

    # with open(dest_file, "w") as my_empty_csv:
    #     pass  # or write something to it already)

    counter = 0
    problematic_disbursements = {}
    successful_disbursements = {}
    problematic_data = {} # where disbursements work but not the df conversion
    all_disbursements = pd.DataFrame()
# looping through all committees; maybe put in a step for testing
    for i in range(0,len(committee_ids)): # optional step
        committee_id = committee_ids[i]
        print(committee_id)
        time.sleep(8) # figure out right amount
        disbursements = get_disbursements(committee_id)
        counter += 1

        if disbursements == []:
            problematic_disbursements[committee_id] = disbursements
            print(counter, 'error: no disbursements')
    #         print(counter)

        else:
            # print(disbursements)
            original_df = pd.DataFrame(disbursements)
            final_df = original_df.copy()

    #         try:
            df_grouped = edit_df(final_df, 'DEM')
            df_grouped = recent_dates(final_df, df_grouped)
            df_grouped = instances(final_df, df_grouped)
            df_grouped = categories(df_grouped, final_df)
            all_disbursements = pd.concat([all_disbursements, df_grouped], axis=0)

            if os.path.exists(dest_file):
                df_grouped.to_csv(dest_file, mode='a', header=False, index = False)
            else:
                df_grouped.to_csv(dest_file, mode='a', header=True, index = False)


            print(counter, "got csv")

    # all_disbursements.to_csv(dest_file)


if __name__ == '__main__':

    all_dem_committees = get_committees('DEM') # use function to get all committees
    final_dem_committees = committees_with_candidate_ids('DEM') # use function to limit to committees with ids
    dem_ids = get_committee_ids('DEM') # use function to get only committee ids
    all_dem_ids = get_candidate_and_committee_ids('DEM') # use function to get candidate id with its committee id
    house_dem = house_money('DEM') # use function to get house data

    download_loop(dem_ids[620:], './disbursements_dem.csv')

    all_rep_committees = get_committees('REP') # use function to get all committees
    final_rep_committees = committees_with_candidate_ids('REP') # use function to limit to committees with ids
    rep_ids = get_committee_ids('REP') # use function to get only committee ids
    all_rep_ids = get_candidate_and_committee_ids('REP') # use function to get candidate id with its committee id
    house_rep = house_money('REP') # use function to get house data

    download_loop(rep_ids, './disbursements_rep.csv')

    #         print(counter)

        #         except:
    #             print("df error")
    #             print(counter)
