import requests
import pandas as pd
import numpy as np
import urllib.request
import time


if __name__ == '__main__':
    while True:
        url = "http://data.wa.gov/resource/wjy5-r8i5.json"
        df = pd.read_json(url, orient='columns')
        df = df[['filer_name','committee_category','contributions_amount','receipt_date','election_year','office','party']]
        df = df[df.election_year == 2018]
        df = df.sort_values(by = ['contributions_amount'], ascending = False)
        df = df.reset_index()
        df = df.drop('index', axis = 1)
        df = df[['filer_name','committee_category','contributions_amount','office','party']]
        df = df.rename(index=str, columns={"filer_name": "Committee Name", "committee_category": "Committee Type", 'contributions_amount': 'Receipt Amount' })
        df = df.rename(index=str, columns={"office": "Office", "party":"Party"})
        df.to_excel('./FEC/STATE_LEVEL_DATA//wa_state_all.xls')
        print('wa_state_all.xls updated. Sleeping 5 days')
        time.sleep(432000)
