import pandas as pd
import urllib.request
import time
import zipfile

def get_ok_spreadsheet():
    url1 = 'https://guardian.ok.gov/PublicSite/Docs/BulkDataDownloads/2018_ContributionLoanExtract.csv.zip'
    url2 = 'https://guardian.ok.gov/PublicSite/Docs/BulkDataDownloads/2017_ContributionLoanExtract.csv.zip'
    cofile = urllib.request.URLopener()
    cofile.retrieve(url1, "./ok2018_ContributionData.zip")
    cofile.retrieve(url2, "./ok2017_ContributionData.zip")

    zip_ref = zipfile.ZipFile("./ok2018_ContributionData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref = zipfile.ZipFile("./ok2017_ContributionData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref.close()

    df1 = pd.read_csv('2017_ContributionLoanExtract.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df2 = pd.read_csv('2018_ContributionLoanExtract.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df3 = pd.concat([df1, df2])
    print(list(df3))

    cnames = [r['Committee Name'] if r['Committee Name'] != ' ' else r['Candidate Name'] for i,r in df3.iterrows()]
    df3['Committee Name'] = cnames
    df3 = df3.groupby(['Committee Name', 'Committee Type'])[['Receipt Amount']].sum()
    df3 = df3.sort_values('Receipt Amount', ascending=False)
    df3.to_excel('./FEC/STATE_LEVEL_DATA/ok_state_all.xls')

if __name__ == '__main__':

    while True:

        get_ok_spreadsheet()
        print('ok_state_all.xls updated. Sleeping 5 days')
        time.sleep(432000)
