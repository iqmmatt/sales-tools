import pandas as pd
import urllib.request
import time
import zipfile
from tracer_states import Tstates

def get_spreadsheet(urls):
    dest = './raw_state_data/'
    df = pd.DataFrame()
    for url in urls:
        fname = url.split('/')[-1]
        cofile = urllib.request.URLopener()
        cofile.retrieve(url, dest+fname)
        # cofile.retrieve(url2, "./al2017_ContributionData.zip")

        zip_ref = zipfile.ZipFile(dest+fname, 'r')
        zip_ref.extractall(dest)
    # zip_ref = zipfile.ZipFile("./al2017_ContributionData.zip", 'r')
    # zip_ref.extractall('./')
        print(fname[:-4])
        zip_ref.close()
        tdf = pd.read_csv(dest+fname[:-4], error_bad_lines=False, encoding = "ISO-8859-1")
        df = pd.concat([df, tdf])

    # df2 = pd.read_csv('2018_CashContributionsExtract.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    # df3 = pd.concat([df1, df2])
    cols = [x.upper() for x in list(df)]
    print(df.head())

    for col in cols:
        if "AMOUNT" in col:
            cols[cols.index(col)] = "AMOUNT"
        elif " " in col:
            cols[cols.index(col)] = col.replace(' ', '')
            # print(col)
    df.columns = cols
    # print(df.head())
    cnames = [r['COMMITTEENAME'] if r['COMMITTEENAME'] != ' ' else r['CANDIDATENAME'] for i,r in df.iterrows()]
    df['COMMITTEENAME'] = cnames
    df = df.groupby(['COMMITTEENAME', 'COMMITTEETYPE'])[['AMOUNT']].sum()
    print(df.head())
    df = df.sort_values('AMOUNT', ascending=False)
    # print(df.head())

    return df

def make_cash_on_hand_df(cdf, sdf):
    """
    cdf = DataFrame of contributions to committees
    sdf = DataFrame of expenditures by committees
    """
    cdf.columns = ['CONTRIBUTIONS']
    sdf.columns = ['DISBURSEMENTS']

    df = pd.merge(cdf.reset_index(), sdf.reset_index()[['COMMITTEENAME', 'DISBURSEMENTS']], on='COMMITTEENAME', how='left', suffixes=('_C', '_S'))
    print(df.head())
    df ['ON_HAND'] = df['CONTRIBUTIONS'] - df['DISBURSEMENTS']
    df = df.sort_values('ON_HAND', ascending=False)
    print(df.head())
    return df

def export_worksheet(df, file_name):
    """
    df = DataFrame that has been assembled by make_cash_on_hand_df
    """
    writer = pd.ExcelWriter(file_name, engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Sheet1')
    workbook  = writer.book
    worksheet = writer.sheets['Sheet1']
    worksheet.set_column('B:B', 50)
    worksheet.set_column('C:C', 25)
    worksheet.set_column('D:F', 15, workbook.add_format({'num_format': '#,##0.00'}))
    writer.save()
    print(file_name, ' is updated. Sleeping 5 days')
    # df3.to_excel('./FEC/STATE_LEVEL_DATA/al_state_all.xls')

def get_co_disbursements_to_categorize():

    """
    processing: function gets data from CO TRACER on disbursements
    output: raw dataframe to categorize disbursements
    """


    url1 = 'http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2018_ExpenditureData.csv.zip'
    url2 = 'http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2017_ExpenditureData.csv.zip'
    cofile = urllib.request.URLopener()
    cofile.retrieve(url1, "./co2018_ExpenditureData.zip")
    cofile.retrieve(url2, "./co2017_ExpenditureData.zip")

    zip_ref = zipfile.ZipFile("./co2018_ExpenditureData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref = zipfile.ZipFile("./co2017_ExpenditureData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref.close()

    df1 = pd.read_csv('2017_ExpenditureData.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df2 = pd.read_csv('2018_ExpenditureData.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df3 = pd.concat([df1, df2])
    #df3 = df3.groupby(['CommitteeName', 'CommitteeType'])[['ExpenditureAmount']].sum()
    df3 = df3.sort_values('ExpenditureAmount', ascending=False)
    df3 = df3.reset_index()
    df3 = df3.drop('index', axis = 1)

    return df3



def get_disbursement_categories():

    """
    processing: function calls get_co_cash_on_hand and
    get_co_disbursements_to_categorize()
    keeps only committees with cash>=25000
    keeps only disbursements in relevant categories

    output: grouped df
    """


    df_raw = get_co_cash_on_hand()
    df_grouped = df_raw.loc[df_raw['CashOnHand'] >= 25000] #only keep 'rich' committees
    relevant_committees = list(df_grouped['CommitteeName'])
    df_to_categorize = get_co_disbursements_to_categorize()
    df_to_categorize = df_to_categorize.loc[df_to_categorize['CommitteeName'].isin(relevant_committees)]
    df = df_to_categorize[['ExpenditureAmount','ExpenditureDate', 'LastName','RecordID','ExpenditureType','CommitteeName','CandidateName']]
    df = df.loc[df['ExpenditureType'].isin(['Consultant & Professional Services','Advertising','Other'])]
    df = df.reset_index()
    df = df.drop('index', axis = 1)
    df_grouped_by_rec = df.groupby(['CommitteeName','ExpenditureType','LastName'])[['ExpenditureAmount']].sum()
    df_grouped_by_rec = df_grouped_by_rec.reset_index()
    df_grouped_by_date = df.groupby(['CommitteeName','ExpenditureType','LastName'])[['ExpenditureDate']].max()
    df_grouped_by_date = df_grouped_by_date.reset_index()
    recent_dates = list(df_grouped_by_date['ExpenditureDate'])
    df_grouped_by_rec['MostRecentDate'] = recent_dates
    return df_grouped_by_rec

if __name__ == '__main__':

    while True:
        # con_urls = ['https://guardian.ok.gov/PublicSite/Docs/BulkDataDownloads/2018_ContributionLoanExtract.csv.zip',
        #             'https://guardian.ok.gov/PublicSite/Docs/BulkDataDownloads/2017_ContributionLoanExtract.csv.zip']
        # cdf = get_al_spreadsheet(con_urls)
        #
        # print(cdf.head())
        #
        # spend_urls = ['https://guardian.ok.gov/PublicSite/Docs/BulkDataDownloads/2018_ExpenditureExtract.csv.zip',
        #               'https://guardian.ok.gov/PublicSite/Docs/BulkDataDownloads/2017_ExpenditureExtract.csv.zip']
        #
        # sdf = get_al_spreadsheet(spend_urls)
        #
        # print(sdf.head())
        #
        # df = make_cash_on_hand_df(cdf, sdf)

        for state in Tstates.keys():
            sdict = Tstates[state]

            con_urls = sdict['contributions']
            cdf = get_spreadsheet(con_urls)

            spend_urls = sdict['disbursements']
            sdf = get_spreadsheet(spend_urls)

            df = make_cash_on_hand_df(cdf, sdf)

            file_name = sdict['file_name']
            export_worksheet(df, file_name)





        # df.to_excel('ok_state_all.xls')



        # print(df.head())

        time.sleep(432000)
