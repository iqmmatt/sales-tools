import pandas as pd
import urllib.request
import time
import zipfile

def get_al_spreadsheet():
    url1 = 'http://fcpa.alabamavotes.gov/PublicSite/Docs/BulkDataDownloads/2018_CashContributionsExtract.csv.zip'
    url2 = 'http://fcpa.alabamavotes.gov/PublicSite/Docs/BulkDataDownloads/2017_CashContributionsExtract.csv.zip'
    cofile = urllib.request.URLopener()
    cofile.retrieve(url1, "./al2018_ContributionData.zip")
    cofile.retrieve(url2, "./al2017_ContributionData.zip")

    zip_ref = zipfile.ZipFile("./al2018_ContributionData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref = zipfile.ZipFile("./al2017_ContributionData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref.close()

    df1 = pd.read_csv('2017_CashContributionsExtract.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df2 = pd.read_csv('2018_CashContributionsExtract.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df3 = pd.concat([df1, df2])
    cnames = [r['CommitteeName'] if r['CommitteeName'] != ' ' else r['CandidateName'] for i,r in df3.iterrows()]
    df3['CommitteeName'] = cnames
    df3 = df3.groupby(['CommitteeName', 'CommitteeType'])[['ContributionAmount']].sum()
    df3 = df3.sort_values('ContributionAmount', ascending=False)

    df3.to_excel('./FEC/STATE_LEVEL_DATA/al_state_all.xls')

if __name__ == '__main__':

    while True:

        get_al_spreadsheet()
        print('al_state_all.xls updated. Sleeping 5 days')
        time.sleep(432000)
