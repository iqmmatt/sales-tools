import pandas as pd
import requests
import time

def get_ia_dataframe():
    base_url = 'https://data.iowa.gov/resource/fbcf-6uqr.json?$limit=50000&$where=date%3E%222017-01-01T00:00:00.000%22&$offset='
    working = True
    df = pd.DataFrame()
    k = 0
    while working == True:
        url = base_url + str(k)
        df2 = pd.read_json(requests.get(url).content)
        if len(df2) < 50000:
            working = False
        df = pd.concat([df, df2])
        k += 50000
        print(k)

    df = df.groupby('committee_nm')[['amount']].sum().sort_values('amount', ascending=False)
    df.to_excel('./FEC/STATE_LEVEL_DATA/ia_state_all.xls')

if __name__ == '__main__':

    while True:

        get_ia_dataframe()
        print('ia_state_all.xls updated. Sleeping 5 days')
        time.sleep(432000)
