import pandas as pd
import urllib.request
import time
import zipfile
import os


def get_co_contributions():
    url1 = 'http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2018_ContributionData.csv.zip'
    url2 = 'http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2017_ContributionData.csv.zip'
    cofile = urllib.request.URLopener()
    cofile.retrieve(url1, "./co2018_ContributionData.zip")
    cofile.retrieve(url2, "./co2017_ContributionData.zip")

    zip_ref = zipfile.ZipFile("./co2018_ContributionData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref = zipfile.ZipFile("./co2017_ContributionData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref.close()

    df1 = pd.read_csv('2017_ContributionData.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df2 = pd.read_csv('2018_ContributionData.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df3 = pd.concat([df1, df2])
    df3 = df3.groupby(['CommitteeName', 'CommitteeType'])[['ContributionAmount']].sum()
    df3 = df3.sort_values('ContributionAmount', ascending=False)
    df3 = df3.reset_index()
    
    return df3


def get_co_disbursements():
    url1 = 'http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2018_ExpenditureData.csv.zip'
    url2 = 'http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2017_ExpenditureData.csv.zip'
    cofile = urllib.request.URLopener()
    cofile.retrieve(url1, "./co2018_ExpenditureData.zip")
    cofile.retrieve(url2, "./co2017_ExpenditureData.zip")

    zip_ref = zipfile.ZipFile("./co2018_ExpenditureData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref = zipfile.ZipFile("./co2017_ExpenditureData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref.close()

    df1 = pd.read_csv('2017_ExpenditureData.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df2 = pd.read_csv('2018_ExpenditureData.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df3 = pd.concat([df1, df2])
    df3 = df3.groupby(['CommitteeName', 'CommitteeType'])[['ExpenditureAmount']].sum()
    df3 = df3.sort_values('ExpenditureAmount', ascending=False)
    df3 = df3.reset_index()
    
    return df3


def get_co_cash_on_hand():
    df_expenditures = get_co_disbursements()
    df_contributions = get_co_contributions()
    df = pd.merge(df_contributions, df_expenditures, on=['CommitteeName', 'CommitteeType'])
    df['CashOnHand'] = df['ContributionAmount'] - df['ExpenditureAmount']
    pd.options.display.float_format = '{:,.2f}'.format
    df = df.sort_values('CashOnHand', ascending=False)
    df = df.reset_index()
    df = df.drop('index', axis = 1)
    
    return df

def saving_file():
    path = './FEC/STATE_LEVEL_DATA/CO'
    folder = './FEC/STATE_LEVEL_DATA/'
    newpath = r'./FEC/STATE_LEVEL_DATA/CO/'

    if not os.path.exists(newpath):
        os.makedirs(newpath)

    ext_bit = ".xls"
    file_name = folder + 'co_state_all' + ext_bit
    
    df = get_co_cash_on_hand()
    
    df.to_excel(file_name)
    

if __name__ == '__main__':
    while True:
        saving_file()
        print('co_state_all.xls updated. Sleeping 5 days')
        time.sleep(432000)
