import requests
import pandas as pd
import numpy as np
import time
import os
from datetime import datetime


def get_committees():
    '''
    processing:
    getting all union/worker committees data from API;

    no input required 
    output:function will return a list of committee ids;

    '''

    # building the url
    base_url = 'https://api.open.fec.gov/v1/committees'
    committee_search = '?cycle=2018&organization_type=L&sort=name'
    api_key = 'LWF33ctqM6qedjTWbDVQbfA3MZ3YrWQANcAmJbbQ'
    key_bit = '&api_key=' + api_key
    
    # looping through the api
    committee_list = []
    
    for i in range(1, 8): 
        try:
            full_url = base_url + committee_search + '&per_page=100' + '&page=' + str(i) + key_bit 
            results = requests.get(full_url)
            
            committee_list.extend(results.json()['results'])

        except:
            pass
        
    committee_ids = []
    for i in range(0, len(committee_list)):
        committee_ids.append(committee_list[i]['committee_id'])
    committee_ids
        
    return committee_ids
    

def get_disbursements(committee_id):

    '''
    committee_id = string for the unique id for each committee

    processing:
    getting disbursements from API for each committee;

    input: function requires committee_id as string;
    output: function returns a list 
            that contains a dict for the committee's disbursements



    '''

    # building the url
    base_url = 'https://api.open.fec.gov/v1/schedules/schedule_b/?'
    committee_search = '&committee_id='
    api_key = 'LWF33ctqM6qedjTWbDVQbfA3MZ3YrWQANcAmJbbQ'
    key_bit = '&api_key=' + api_key
    
    # creating a list for the results
    disbursements_list = []
    
    full_url = base_url + '&per_page=100' + key_bit + committee_search + committee_id + '&two_year_transaction_period=2018'
    results = requests.api.get(full_url)
    status_code = results.status_code
    
    if status_code != 200:
        return 'error'

    while type(results.json()['results']) != list:
        print("Error", type(results.json()['results']))
        time.sleep(5)
        results = requests.api.get(full_url)

    disbursements_list.extend(results.json()['results'])
    
    return disbursements_list



def put_disbursements_in_list():
    '''
    processing:
    function puts the disbursements API results 
    in a list so that you can pull each committee individually
    
    no input
    output: list of all disbursements for all committees

    calls: get_committees(), get_disbursements()

    '''
    committees = get_committees()
    all_disbursements = []
    for i in range(0, len(committees)):
        committee_id = committees[i]
        one_committee = get_disbursements(committee_id)
        all_disbursements.append(one_committee)    
    return all_disbursements



def get_df_and_edit(one_committee):
    '''
    one_committee = dict of disbursements of 1 committee

    processing:
    function to edit dfs once they are initially created; 
    gets called in master processing function;
    creates df for each committee's disbursements;
    edits df

    input: requires a disbursement dictionary as input
    output: editted df

    '''


    df = pd.DataFrame(one_committee)
    edit_df = df[['committee','committee_id','cycle','disbursement_amount','disbursement_date','disbursement_description','entity_type','recipient_name', 'transaction_id']]
    committee_name = edit_df['committee'][0]['name']
    edit_df['committee_name'] = committee_name
    committee_type = edit_df['committee'][0]['committee_type_full']
    edit_df['committee_type'] = committee_type
    state = edit_df['committee'][0]['state']
    edit_df['state'] = state
    city = edit_df['committee'][0]['city']
    edit_df['city'] = city
    org_type = edit_df['committee'][0]['organization_type_full']
    edit_df['organization_type'] = org_type
    edit_df = edit_df.drop('committee', axis=1)
    return edit_df



def df_to_csv(df):
    '''
    df: final df returned from categories(df)

    processing:
    funtion to save dfs into spredsheets 
    and to create a system of files

    input: finalized df
    output: excel files


    '''
    
    for j in df['state']:
        if j != None:
            state = j
            break
        else:
            state = None

    path = 'FEC/UNIONS/' + str(state)
    folder = 'FEC/UNIONS/'  + str(state) + '/'         
    newpath = r'FEC/UNIONS/' + str(state) 
    
    if not os.path.exists(newpath):
        os.makedirs(newpath)
        
    committee_bit = df['committee_id'][0]  # careful to have name from previous functions
    ext_bit = ".xls"
    file_name = folder + str(committee_bit) + ext_bit
    
    try:
        csv = df.to_excel(file_name)
        return csv
    except:
        return 'error'




def categories(df):
    '''

    df: dataframe returned by get_df_and_edit(one_committee)

    processing:
    creates a column for descriptions by specified keywords; 
    edits the modified df

    input: df returned by get_df_and_edit(one_committee)
    output: final edit of disbursements df

    '''

    # getting the descriptions
    descriptions = df['disbursement_description']
    descriptions

    # holding them in a list
    categories = []
    for i in descriptions:
        categories.append(i)
    categories

    # iterating through all the descriptions and labeling them for category based on keywords
    # returning a list with a label for each description
    relevant = []
    for i in categories:
        try:
            description = i.lower()
        except:
            description = 'None'
        words = description.split(' ')
        choice = []
        category = []
        for word in words:
            if word in ['media','advertising', 'tv', 'digital','radio', 'online', 'consulting', 'media.','advertising.', 'tv.', 'digital.','radio.', 'online.', 'consulting.']:
                adding = 'yes'
                choice.append(adding)
                category.append(word)
            else:
                adding = 'no'
                choice.append(adding)
        if 'yes' in choice:
            relevant.append(category[0])
        else:
            relevant.append("other")
    relevant

    # adding a column with the labels
    df['category'] = relevant
    df
    
    return df



def go_through_all_committees_and_process():
    '''
    processing:
    goes through each committee and creates a df;
    edits df with other functions
    creates file system
    if max disbursement <= 5000, no file will be created

    calls: 
    put_disbursements_in_list(), 
    get_df_and_edit(one_committee), 
    categories(one_df), 
    df_to_csv(one_df_with_categories)

    input: none
    output: list with committee ids 
    for committees that don't meet max disb amount requirement
    '''

    disbursements_list = put_disbursements_in_list()
    print("There are this many committees:", len(disbursements_list))

    counter = 0
    discard = []
    for i in range(0, len(disbursements_list)):
        one_committee = disbursements_list[i]
        if one_committee != []:
            one_df = get_df_and_edit(one_committee)

            if max(one_df['disbursement_amount']) >= 5000:
                keep = True

            else:
                keep = False
                discard.append(one_df['committee_id'][0])

            if keep == True:
                one_df_with_categories = categories(one_df)
                df_to_csv(one_df_with_categories)
                counter += 1
                print(counter)

    return discard



def get_committees_info(discard):
    '''
    discard: list of discarded committee ids
    due to max disb <= 5000

    processing:
    creates master df for all committees
    pulls from API
    only keeps key info

    input: discarded committee ids
    output: spreadsheet for all committees

    '''

    # building the url
    base_url = 'https://api.open.fec.gov/v1/committees'
    committee_search = '?cycle=2018&organization_type=L&sort=name'
    api_key = 'LWF33ctqM6qedjTWbDVQbfA3MZ3YrWQANcAmJbbQ'
    key_bit = '&api_key=' + api_key
    
    # looping through the api
    committee_list = []
    
    for i in range(1, 8): 
        try:
            full_url = base_url + committee_search + '&per_page=100' + '&page=' + str(i) + key_bit 
            results = requests.get(full_url)
            
            committee_list.extend(results.json()['results'])

        except:
            pass
        
    committees_df = pd.DataFrame(committee_list)
        
    for j in range(0, len(discard)):
        errant_id = discard[j]
        committees_df = committees_df[committees_df.committee_id != errant_id]
    committees_df = committees_df.reset_index()

    
    # create a df with only relevant 
    master_df = committees_df[['name','organization_type_full','state','committee_id','committee_type_full','last_file_date']]
    
    states = list(master_df['state'])
    ids = list(master_df['committee_id'])
    filepaths = []
    for i in range(0,len(master_df)):
        state = states[i]
        committee_id = ids[i]
        filepath = "FEC/UNIONS/" + str(state) + "/" + str(committee_id)
        filepaths.append(filepath)
    master_df['file_path'] = filepaths
    
    return master_df


if __name__ == '__main__':

    print('starting union scraper')
    while True:
        discard = go_through_all_committees_and_process()
        master_df = get_committees_info(discard)
        master_df.to_excel("FEC/Unions.xlsx")
        print("ALL DONE SCRAPING UNION DATA!!!")
        print("Last update at: ", datetime.today())
        time.sleep(72000)
