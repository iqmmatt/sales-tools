import pandas as pd
import urllib.request
import time
import zipfile

def get_co_spreadsheet():
    url1 = 'http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2018_ContributionData.csv.zip'
    url2 = 'http://tracer.sos.colorado.gov/PublicSite/Docs/BulkDataDownloads/2017_ContributionData.csv.zip'
    cofile = urllib.request.URLopener()
    cofile.retrieve(url1, "./co2018_ContributionData.zip")
    cofile.retrieve(url2, "./co2017_ContributionData.zip")

    zip_ref = zipfile.ZipFile("./co2018_ContributionData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref = zipfile.ZipFile("./co2017_ContributionData.zip", 'r')
    zip_ref.extractall('./')
    zip_ref.close()

    df1 = pd.read_csv('2017_ContributionData.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df2 = pd.read_csv('2018_ContributionData.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    df3 = pd.concat([df1, df2])
    df3 = df3.groupby(['CommitteeName', 'CommitteeType'])[['ContributionAmount']].sum()
    df3 = df3.sort_values('ContributionAmount', ascending=False)

    df3.to_excel('./FEC/STATE_LEVEL_DATA/co_state_all.xls')

if __name__ == '__main__':

    while True:

        get_co_spreadsheet()
        print('co_state_all.xls updated. Sleeping 5 days')
        time.sleep(432000)
