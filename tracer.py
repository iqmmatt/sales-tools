import pandas as pd
import urllib.request
import time
import zipfile
from tracer_states import Tstates
import os


def get_spreadsheet(urls):
    dest = './raw_state_data/'
    df = pd.DataFrame()
    for url in urls:
        fname = url.split('/')[-1]
        cofile = urllib.request.URLopener()
        cofile.retrieve(url, dest+fname)
        # cofile.retrieve(url2, "./al2017_ContributionData.zip")

        zip_ref = zipfile.ZipFile(dest+fname, 'r')
        zip_ref.extractall(dest)
    # zip_ref = zipfile.ZipFile("./al2017_ContributionData.zip", 'r')
    # zip_ref.extractall('./')
        print(fname[:-4])
        zip_ref.close()
        tdf = pd.read_csv(dest+fname[:-4], error_bad_lines=False, encoding = "ISO-8859-1")
        df = pd.concat([df, tdf])

    # df2 = pd.read_csv('2018_CashContributionsExtract.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    # df3 = pd.concat([df1, df2])
    cols = [x.upper() for x in list(df)]
    #print(df.head())

    for col in cols:
        if "AMOUNT" in col:
            cols[cols.index(col)] = "AMOUNT"
        elif " " in col:
            cols[cols.index(col)] = col.replace(' ', '')
            # print(col)
    df.columns = cols
    # print(df.head())
    cnames = [r['COMMITTEENAME'] if r['COMMITTEENAME'] != ' ' else r['CANDIDATENAME'] for i,r in df.iterrows()]
    df['COMMITTEENAME'] = cnames
    df = df.groupby(['COMMITTEENAME', 'COMMITTEETYPE'])[['AMOUNT']].sum()
    #print(df.head())
    df = df.sort_values('AMOUNT', ascending=False)
    # print(df.head())

    return df


def get_spreadsheet_disb(urls):
    dest = './raw_state_data/'
    df = pd.DataFrame()
    for url in urls:
        fname = url.split('/')[-1]
        cofile = urllib.request.URLopener()
        cofile.retrieve(url, dest+fname)
        # cofile.retrieve(url2, "./al2017_ContributionData.zip")

        zip_ref = zipfile.ZipFile(dest+fname, 'r')
        zip_ref.extractall(dest)
    # zip_ref = zipfile.ZipFile("./al2017_ContributionData.zip", 'r')
    # zip_ref.extractall('./')
        #print(fname[:-4])
        zip_ref.close()
        tdf = pd.read_csv(dest+fname[:-4], error_bad_lines=False, encoding = "ISO-8859-1")
        df = pd.concat([df, tdf])

    # df2 = pd.read_csv('2018_CashContributionsExtract.csv', error_bad_lines=False, encoding = "ISO-8859-1")
    # df3 = pd.concat([df1, df2])
    cols = [x.upper() for x in list(df)]
    #print(df.head())

    for col in cols:
        if "AMOUNT" in col:
            cols[cols.index(col)] = "AMOUNT"
        elif " " in col:
            cols[cols.index(col)] = col.replace(' ', '')
            # print(col)
    df.columns = cols
    # print(df.head())
    cnames = [r['COMMITTEENAME'] if r['COMMITTEENAME'] != ' ' else r['CANDIDATENAME'] for i,r in df.iterrows()]
    df['COMMITTEENAME'] = cnames
    #df = df.groupby(['COMMITTEENAME', 'COMMITTEETYPE'])[['AMOUNT']].sum()
    #print(df.head())
    #df = df.sort_values('AMOUNT', ascending=False)
    # print(df.head())

    return df



def make_cash_on_hand_df(cdf, sdf):
    """
    cdf = DataFrame of contributions to committees
    sdf = DataFrame of expenditures by committees
    """
    cdf.columns = ['CONTRIBUTIONS']
    sdf.columns = ['DISBURSEMENTS']

    df = pd.merge(cdf.reset_index(), sdf.reset_index()[['COMMITTEENAME', 'DISBURSEMENTS']], on='COMMITTEENAME', how='left', suffixes=('_C', '_S'))
    #print(df.head())
    df ['ON_HAND'] = df['CONTRIBUTIONS'] - df['DISBURSEMENTS']
    df = df.sort_values('ON_HAND', ascending=False)
    #print(df.head())
    return df



def get_disbursement_categories(cdf, sdf, spend_urls):
    """
    cdf = df with contributions
    sdf = df with spending
    spend_urls = urls with location of raw files, refers to value in Tstates {}

    processing: returns df only with spending in relevant categories
    			adds most recent date to recipient
    """


    df_raw = make_cash_on_hand_df(cdf, sdf)
    df_grouped = df_raw.loc[df_raw['ON_HAND'] >= 25000] #only keep 'rich' committees
    relevant_committees = list(df_grouped['COMMITTEENAME'])
    df_to_categorize = get_spreadsheet_disb(spend_urls)
    df_to_categorize = df_to_categorize.loc[df_to_categorize['COMMITTEENAME'].isin(relevant_committees)]
    df = df_to_categorize[['AMOUNT','EXPENDITUREDATE', 'LASTNAME','EXPENDITURETYPE','COMMITTEENAME','CANDIDATENAME']]
    df = df.loc[df['EXPENDITURETYPE'].isin(['Consultant & Professional Services','Advertising','Other'])]
    df['EXPENDITUREDATE'] = pd.to_datetime(df['EXPENDITUREDATE'])
    df = df.reset_index()
    df = df.drop('index', axis = 1)
    df_grouped_by_rec = df.groupby(['COMMITTEENAME','EXPENDITURETYPE','LASTNAME'])[['AMOUNT']].sum()
    df_grouped_by_rec = df_grouped_by_rec.reset_index()
    df_grouped_by_date = df.groupby(['COMMITTEENAME','EXPENDITURETYPE','LASTNAME'])[['EXPENDITUREDATE']].max()
    df_grouped_by_date = df_grouped_by_date.reset_index()
    recent_dates = list(df_grouped_by_date['EXPENDITUREDATE'])
    df_grouped_by_rec['MostRecentDate'] = recent_dates
    return df_grouped_by_rec



def get_all_disb_categories(cdf, sdf, spend_urls): #if there's no $ for relevant categories

    """

    cdf = df with contributions
    sdf = df with spending
    spend_urls = urls with location of raw files, refers to value in Tstates {}
    get_disbursement_categories(cdf, sdf, spend_urls)


    processing: returns df with spending in all categories
    			adds most recent date to recipient

    """
    df_raw = make_cash_on_hand_df(cdf, sdf)
    df_grouped = df_raw.loc[df_raw['ON_HAND'] >= 25000] #only keep 'rich' committees
    relevant_committees = list(df_grouped['COMMITTEENAME'])
    df_to_categorize = get_spreadsheet_disb(spend_urls)
    df_to_categorize = df_to_categorize.loc[df_to_categorize['COMMITTEENAME'].isin(relevant_committees)]
    df = df_to_categorize[['AMOUNT','EXPENDITUREDATE', 'LASTNAME','EXPENDITURETYPE','COMMITTEENAME','CANDIDATENAME']]
    # df = df.loc[df['EXPENDITURETYPE'].isin(['Consultant & Professional Services','Advertising','Other'])]
    df['EXPENDITUREDATE'] = pd.to_datetime(df['EXPENDITUREDATE'])
    df = df.reset_index()
    df = df.drop('index', axis = 1)
    df_grouped_by_rec = df.groupby(['COMMITTEENAME','EXPENDITURETYPE','LASTNAME'])[['AMOUNT']].sum()
    df_grouped_by_rec = df_grouped_by_rec.reset_index()
    df_grouped_by_date = df.groupby(['COMMITTEENAME','EXPENDITURETYPE','LASTNAME'])[['EXPENDITUREDATE']].max()
    df_grouped_by_date = df_grouped_by_date.reset_index()
    recent_dates = list(df_grouped_by_date['EXPENDITUREDATE'])
    df_grouped_by_rec['MostRecentDate'] = recent_dates
    return df_grouped_by_rec



def export_worksheet(df, file_name):
    """
    df = DataFrame that has been assembled by get_disbursement_categories()
    """
    writer = pd.ExcelWriter(file_name, engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Sheet1')
    workbook  = writer.book
    worksheet = writer.sheets['Sheet1']
    worksheet.set_column('B:B', 50)
    worksheet.set_column('C:C', 25)
    worksheet.set_column('D:D', 25)
    worksheet.set_column('E:F', 25, workbook.add_format({'num_format': '#,##0.00'}))
    writer.save()



def get_files(df, state):

    """
    processing: function calls get_disbursement_categories()
    and export_worksheet(df_grouped, file_name)
    get_co_disbursements_to_categorize()
    saves an xlsx file for each committee in specified folder


    output: grouped df
    """

    committees = list(df['COMMITTEENAME'].unique())

    path = './FEC/STATE_LEVEL_DATA/' + str(state)
    folder = './FEC/STATE_LEVEL_DATA/' + str(state) + '/'
    newpath = r'./FEC/STATE_LEVEL_DATA/' + str(state) + '/'

    if not os.path.exists(newpath):
        os.makedirs(newpath)

    for i in range(0, len(committees)):
        df_grouped = df.loc[df['COMMITTEENAME'] == committees[i]]
        df_grouped = df_grouped.reset_index()
        df_grouped = df_grouped.drop('index', axis = 1)
        df_grouped = df_grouped.rename(index=str, columns={"LASTNAME": "RECIPIENT"})

        # Saving to file
        ext_bit = ".xlsx"
        one = str(committees[i])

        if "/" in one:
            continue
        else:
            file_name = folder + one + ext_bit
            export_worksheet(df_grouped, file_name)



if __name__ == '__main__':

    while True:

        for state in Tstates.keys():
            sdict = Tstates[state]

            con_urls = sdict['contributions']
            cdf = get_spreadsheet(con_urls)

            spend_urls = sdict['disbursements']
            sdf = get_spreadsheet(spend_urls)
            ddf = get_spreadsheet_disb(spend_urls)

            df = make_cash_on_hand_df(cdf, sdf)

            newpath = r'./FEC/STATE_LEVEL_DATA/'

            if not os.path.exists(newpath):
                os.makedirs(newpath)

            file_name = sdict['file_name']
            export_worksheet(df, './FEC/STATE_LEVEL_DATA/' + file_name)

            df_categorized = get_disbursement_categories(cdf, sdf, spend_urls)

            if len(df_categorized) == 0:

                df_categorized = get_all_disb_categories(cdf, sdf, spend_urls)

            else:

                df_categorized = get_disbursement_categories(cdf, sdf, spend_urls)


            get_files(df_categorized, state)

        print("Got them!")
        time.sleep(432000)
