import re
import smtplib
import dns.resolver

def smtp_email_check(email):
    # Address used for SMTP MAIL FROM command
    fromAddress = 'app@iqm.com'
    addressToVerify = email
    # Get domain for DNS lookup
    splitAddress = addressToVerify.split('@')
    domain = str(splitAddress[1])
#     print('Domain:', domain)

    # MX record lookup
    records = dns.resolver.query(domain, 'MX')
    mxRecord = records[0].exchange
    mxRecord = str(mxRecord)

    print(mxRecord)
    # SMTP lib setup (use debug level for full output)
    server = smtplib.SMTP()
    server.set_debuglevel(0)

    # SMTP Conversation
    server.connect(mxRecord)
    server.helo(server.local_hostname) ### server.local_hostname(Get local server hostname)
    server.mail(fromAddress)
    code, message = server.rcpt(str(addressToVerify))
    server.quit()

    # Assume SMTP response 250 is success
    if code == 250:
        return True
    else:
        return False

def check_all_possibilities(first, last, domain):

    first = first.strip().lower()
    last = last.strip().lower()

    variants = [
        first,
        last,
        first + '.' + last,
        first[0] + '.' + last,
        first[0] + last,
        first[0] + last[0]
    ]

    addys = [x + "@" + domain for x in variants]

    working = []

    for addy in addys:
        ck = smtp_email_check(addy)
        if ck == True:
            working.append(addy)

    if len(working) > 0:
        return working
    else:
        return ['No working email address for this person']
