from flask import Flask, request, render_template, session
from email_validator import check_all_possibilities
app = Flask(__name__)

#Post Data Request:
#@app.route("/")
@app.route('/', methods=['GET'])
def index():
    '''
    Creates an index page with plaintext submission.
    '''
    return render_template('home.html')
# Register Submission into Database

@app.route('/results', methods=['GET'])
def results():
    first = request.args.get('first')
    last = request.args.get('last')
    domain = request.args.get('domain')

    print(first, last, domain)

    results = check_all_possibilities(first, last, domain)

    return render_template('home.html',
                            first = first,
                            last = last,
                            domain = domain,
                            results=results)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
