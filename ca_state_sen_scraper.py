import pandas as pd
import urllib.request
import time

url='http://powersearch.sos.ca.gov/download_csv.php?w=WHERE+smry_offices.RecipientCandidateOffice+%3D+%3F+AND+contributions_search.CandidateContribution+%3D+%27Y%27+AND+%28contributions_search.ElectionCycle+%3D+%3F%29&d=a%3A2%3A%7Bi%3A0%3Bs%3A12%3A%22State+Senate%22%3Bi%3A1%3Bs%3A4%3A%222017%22%3B%7D&c=a%3A6%3A%7Bs%3A16%3A%2200Contributor%28s%29%22%3Bs%3A3%3A%22All%22%3Bs%3A19%3A%2201Contributor_State%22%3Bs%3A3%3A%22All%22%3Bs%3A14%3A%2202Recipient%28s%29%22%3Bs%3A14%3A%22All+candidates%22%3Bs%3A31%3A%2207Contribution_Dates_and_Cycles%22%3Bs%3A0%3A%22%22%3Bs%3A18%3A%2203Recipient_Office%22%3Bs%3A12%3A%22State+Senate%22%3Bs%3A21%3A%2209Contribution_Cycles%22%3Bs%3A4%3A%222017%22%3B%7D'

# with urllib.request.urlopen("http://www.python.org") as url:
#     cafile = url.read()

if __name__ == '__main__':

    while True:

        cafile = urllib.request.URLopener()
        cafile.retrieve(url, "./ca_senate.csv")
        casen = pd.read_csv("./ca_senate.csv")
        cagrouped = casen.groupby('Recipient Committee').sum()[['Amount']].sort_values('Amount', ascending=False)
        cagrouped.to_excel('./FEC/STATE_LEVEL_DATA/ca_state_senate.xls')
        print('ca_state_senate.xls updated. Sleeping 5 days')
        time.sleep(432000)
