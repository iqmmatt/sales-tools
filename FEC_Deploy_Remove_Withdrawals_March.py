''' All code combined both for candidates and disbursements data fetching'''

import requests
import pandas as pd
import numpy as np
import time
import os
from datetime import datetime
from bs4 import BeautifulSoup

'''
Purpose of this code is to fetch data for each candidatate in the 2018 cycle
    The info should include:
        - candidate name
        - party
        - state
        - district
        - ending cash on hand
        - most recent date
        - committee id
        - filepath to disbursements
    Organize the data in a spreadsheet for all candidates
'''


# getting all committees disregarding categories and presence of candidate id
def get_committees(party):
    # building the url
    base_url = 'https://api.open.fec.gov/v1/'
    committee_search = 'committees/?sort=name&cycle=2018'
    filters = '&party=' + str(party)
    api_key = 'VzismHMizkoQdnROPC9CSAELkTt6kdVZtpXOD23Q'
    key_bit = '&api_key=' + api_key

    # looping through the api
    committee_list = []

    for i in range(1, 30): #because there are around 30 pages
        try:
            full_url = base_url + committee_search + '&per_page=100' + '&page=' + str(i) + filters + key_bit
            results = requests.get(full_url)
            data_type = type(results.json()['results'])

            while type(results.json()['results']) != list:
                print("Error", type(results.json()['results']))
                time.sleep(5)
                results = requests.api.get(full_url)

            committee_list.extend(results.json()['results'])

        except:
            pass

    return committee_list


# getting only committees with candidate ids
def committees_with_candidate_ids(party):

    party_str = str(party)
    all_committees = get_committees(party_str)

    committees_with_candidate_ids = []

    # loop through all the committees and pull only ones where the candidate_ids list is not empty
    for i in range(0, len(all_committees)):
        if all_committees[i]['candidate_ids'] != []:
            committees_with_candidate_ids.append(all_committees[i])

    return committees_with_candidate_ids


# creating a list with all the committee ids
def get_committee_ids(party):

    party_str = str(party)
    final_committees = committees_with_candidate_ids(party_str)

    committee_ids = []
    for i in range(0, len(final_committees)):
        one_id = final_committees[i]['committee_id']
        committee_ids.append(one_id)
    return committee_ids


# create a dict that has each candidate_id with its corresponding committee_id
def get_candidate_and_committee_ids(party):

    party_str = str(party)
    committees = committees_with_candidate_ids(party_str)
    all_ids = {}
    for i in range(0, len(committees)):
        committee_id = committees[i]['committee_id']
        candidate_id = committees[i]['candidate_ids']
        # all_ids[committee_id] = candidate_id
        if len(candidate_id) == 1:
            all_ids[candidate_id[0]] = committee_id
    return all_ids


# fetching data for House candidates
def house_money(party):
    # building the url
    base_url = 'https://api.open.fec.gov/v1/'
    candidate_search = 'candidates/totals/?cycle=2018&office=H&per_page=100&has_raised_funds=true&party='
    api_key = 'VzismHMizkoQdnROPC9CSAELkTt6kdVZtpXOD23Q'
    key_bit = '&api_key=' + api_key

    # looping through the api
    candidate_finance = []

    for i in range(1, 30):
        try:
            full_url = base_url + candidate_search + party + '&page=' + str(i) + key_bit
            results = requests.api.get(full_url)
            data_type = type(results.json()['results'])

            while type(results.json()['results']) != list:
                print("Error", type(results.json()['results']))
                time.sleep(5)
                results = requests.api.get(full_url)

            candidate_finance.extend(results.json()['results'])

        except:
            pass

    return candidate_finance


# fetching data for House Primaries dates
def house_dates():
    # building the url
    base_url = 'https://api.open.fec.gov/v1/'
    date_search = 'election-dates/?per_page=100&office_sought=H&election_type_id=P&election_year=2018&sort=-election_date'
    api_key = 'VzismHMizkoQdnROPC9CSAELkTt6kdVZtpXOD23Q'
    key_bit = '&api_key=' + api_key

    # looping through the api
    dates = []

    full_url = base_url + date_search + key_bit
    results = requests.api.get(full_url)
    data_type = type(results.json()['results'])

    while type(results.json()['results']) != list:
        print("Error", type(results.json()['results']))
        time.sleep(5)
        results = requests.api.get(full_url)

    dates.extend(results.json()['results'])

    primaries = {}
    for i in dates:
        state = i['election_state']
        date = i['election_date']
        primaries[state] = date

    return primaries


# fetching data for Senate candidates
def senate_money(party):
    # building the url
    base_url = 'https://api.open.fec.gov/v1/'
    candidate_search = 'candidates/totals/?has_raised_funds=true&cycle=2018&office=S'
    filters = '&party='
    api_key = 'VzismHMizkoQdnROPC9CSAELkTt6kdVZtpXOD23Q'
    key_bit = '&api_key=' + api_key

    # looping through the api
    finance = []

    for i in range(1, 30):
        try:
            full_url = base_url + candidate_search + filters + party + '&page=' + str(i) + key_bit + '&per_page=100'
            results = requests.api.get(full_url)
            data_type = type(results.json()['results'])

            while type(results.json()['results']) != list:
                print("Error", type(results.json()['results']))
                time.sleep(5)
                results = requests.api.get(full_url)

            finance.extend(results.json()['results'])

        except:
            pass

    return finance


# fetching data for Senate Primaries dates
def senate_dates():
    # building the url
    base_url = 'https://api.open.fec.gov/v1/'
    date_search = 'election-dates/?per_page=100&office_sought=S&election_type_id=P&election_year=2018&sort=-election_date'
    api_key = 'VzismHMizkoQdnROPC9CSAELkTt6kdVZtpXOD23Q'
    key_bit = '&api_key=' + api_key

    # looping through the api
    dates = []

    full_url = base_url + date_search + key_bit
    results = requests.api.get(full_url)
    data_type = type(results.json()['results'])

    while type(results.json()['results']) != list:
        print("Error", type(results.json()['results']))
        time.sleep(5)
        results = requests.api.get(full_url)

    dates.extend(results.json()['results'])

    primaries = {}
    for i in dates:
        state = i['election_state']
        date = i['election_date']
        primaries[state] = date

    return primaries



'''
Functions to proceess raw data into dataframes
'''

# putting senate data into df and adding columns for most recent date, committee id, office, filepath
def senate_data_to_df(senate, party):

    # creating a dataframe from the raw data
    senate_df = pd.DataFrame(senate)

    # creating a grouped df to add to
    senate_grouped = senate_df.groupby(['candidate_id','name','state']).max()[['cash_on_hand_end_period']]
    senate_grouped = senate_grouped.reset_index()

    if party == 'DEM':
        all_ids = all_dem_ids

    if party == 'REP':
        all_ids = all_rep_ids

    # adding committee ids
    committee_ids = []
    for c in list(senate_grouped['candidate_id']):
        counter = 0
        for d in all_ids.keys():
            if c == d:
                committee_ids.append(all_ids[d])
                counter += 1
        if counter != 1:
            committee_ids.append('NaN')

    senate_grouped['committee_id'] = committee_ids

    # Adding a column with most recent dates

    # converting dates
    senate_df['cash_end_date'] = pd.to_datetime(senate_df['last_file_date'],errors = 'ignore')
    # getting unique candidates
    candidates = senate_df['name'].unique()

    # getting the most recent dates and putting them in a dictionary
    recent = {}
    for i in candidates:
        senate_df[senate_df['name'] == i]['cash_end_date'].max()
        recent[i] = senate_df[senate_df['name'] == i]['cash_end_date'].max()
    recent

    # adding the dates to the df
    last_dates = []
    for i in range(len(senate_df)):
        row = senate_df.iloc[i]
        last_dates.append(recent[row['name']])
    last_dates

    # adding the column to the df
    senate_grouped['most_recent_date'] = last_dates


    # adding columns for office, committee id, cycle
    senate_grouped['cycle'] = senate_df['cycle']
    senate_grouped['office'] = senate_df['office']


    # adding filepaths to the df
    paths = []
    for j in range(0, len(list(senate_grouped['state']))):
        state = (list(senate_grouped['state']))[j]
        committee_id = (list(senate_grouped['committee_id']))[j]
        path = state + '/S/' + committee_id
        paths.append(path)

    # adding the column to the df
    senate_grouped['disbursements_file_path'] = paths


    # adding primary dates to the df
    dates = senate_dates()
    dates_column = []
    for d in range(0, len(list(senate_grouped['state']))):
        state = (list(senate_grouped['state']))[d]
        if state in dates.keys():
            date = dates[state]
            dates_column.append(date)
        else:
            date = None
            dates_column.append(date)


    # adding the column to the df
    senate_grouped['primary_date'] = dates_column


    return senate_grouped



# putting house data into df and adding columns for most recent date, committee id, office, filepath
def house_data_to_df(house, party):

    # creating a dataframe from the raw data
    house_df = pd.DataFrame(house)

    # creating a grouped df to add to
    house_grouped = house_df.groupby(['candidate_id','name','state']).max()[['cash_on_hand_end_period']]
    house_grouped = house_grouped.reset_index()

    if party == 'DEM':
        all_ids = all_dem_ids

    if party == 'REP':
        all_ids = all_rep_ids

    # adding a column with committee ids
    committee_ids = []
    for c in list(house_grouped['candidate_id']):
        counter = 0
        for d in all_ids.keys():
            if c == d:
                committee_ids.append(all_ids[d])
                counter += 1
        if counter != 1:
            committee_ids.append('NaN')

    house_grouped['committee_id'] = committee_ids

    # adding a column with districts
    districts = []
    for b in list(house_grouped['candidate_id']):
        counter = 0
        for g in house:
            if b == g['candidate_id']:
                districts.append(g['district'])
                counter += 1
        if counter != 1:
            districts.append('NaN')

    house_grouped['district'] = districts


    # Adding a column with most recent dates

    # converting dates
    house_df['cash_end_date'] = pd.to_datetime(house_df['last_file_date'],errors = 'ignore')
    # getting unique candidates
    house_candidates = house_df['name'].unique()

    # getting the most recent dates and putting them in a dictionary
    dates = {}
    for i in house_candidates:
        house_df[house_df['name'] == i]['cash_end_date'].max()
        dates[i] = house_df[house_df['name'] == i]['cash_end_date'].max()
    dates

    # adding the dates to the df
    recent_dates = []
    for i in range(len(house_df)):
        row = house_df.iloc[i]
        recent_dates.append(dates[row['name']])
    recent_dates

    # adding the column to the df
    house_grouped['most_recent_date'] = recent_dates


    # adding columns for cycle, office and committee id
    house_grouped['office'] = house_df['office']
    house_grouped['cycle'] = house_df['cycle']


    # adding filepaths to the df
    paths = []
    for f in range(0, len(list(house_grouped['state']))):
        state = (list(house_grouped['state']))[f]
        committee_id = (list(house_grouped['committee_id']))[f]
        path = state + '/H/' + committee_id
        paths.append(path)

    # adding the column to the df
    house_grouped['disbursements_file_path'] = paths


    # adding primary dates to the df
    dates = house_dates()
    dates_column = []
    for d in range(0, len(list(house_grouped['state']))):
        state = (list(house_grouped['state']))[d]
        if state in dates.keys():
            date = dates[state]
            dates_column.append(date)
        else:
            date = None
            dates_column.append(date)


    # adding the column to the df
    house_grouped['primary_date'] = dates_column


    return house_grouped





'''The following procedure executes the following tasks:
1. Get all committees from the API via get_committees.
2. Iterate through the committees and only keep the ones that have a candidate id via committees_with_candidate_ids.
3. Create a list that holds all the filtered committee ids via get_committee_ids.
4. Loop through all the committee ids and do the following:
   - Get the disbursements for each committee via sements(committee_id).
   - Create a df from the data and process it through the df via a set of functions: all_df_operations(disbursements).
   - Save the editted df as a csv.
'''


# getting disbursements for each committee, sorting based on commitee_id, function requires committee_id
def get_disbursements(committee_id):
    # building the url
    base_url = 'https://api.open.fec.gov/v1/schedules/schedule_b/?'
    committee_search = '&committee_id='
    api_key = 'VzismHMizkoQdnROPC9CSAELkTt6kdVZtpXOD23Q'
    key_bit = '&api_key=' + api_key

    # creating a list for the results
    disbursements_list = []

    full_url = base_url + '&per_page=100' + key_bit + committee_search + committee_id + '&two_year_transaction_period=2018'
    results = requests.api.get(full_url)

    while True:

        status_code = results.status_code
        print(status_code)
        # print(results.json())

        while status_code == 502:
            time.sleep(10)
            results = requests.api.get(results.url)
            status_code = results.status_code
            print('api not responding')

        if status_code != 200:
            print('error')
            return 'error'


        # while type(results.json()['results']) != list:
        #     print("Error", type(results.json()['results']))
        #     time.sleep(5)
        #     results = requests.api.get(full_url)
        #     print(results)


        disbursements_list.extend(results.json()['results'])

        time.sleep(5)

        print(len(results.json()['results']))

        if len(results.json()['results']) < 100:
            break

        last_index = results.json()['pagination']['last_indexes']['last_index']

        ext_url = full_url + '&last_index=' + str(last_index)
        results = requests.api.get(ext_url)

    return disbursements_list


# create a df from the disbursements
def create_df(disbursements): # take what sements spits out as input
    if disbursements != 'error':
        df = pd.DataFrame(disbursements)
        return df
    else:
        return 'error'


# edit the df: add columns and group by
def edit_df(final_df, party): # take the newly created fr
    # adding new columns to df
    final_df['party'] = str(party)
    final_df['cycle'] = 2018
    committee_name = final_df['committee'][0]['name']
    final_df['committee_name'] = committee_name
    final_df['candidate_id'] = final_df['candidate_id']
    state = final_df['committee'][0]['state']
    final_df['state'] = state
    final_df['district'] = final_df['candidate_office_district']
    office = final_df['committee'][0]['committee_type']
    final_df['office'] = office


    # grouping to see total disbursement amount to a recipent
    # CHANGE IN FOLLOWING LINE
    df_grouped = final_df.groupby(['committee_id','recipient_name', 'disbursement_description','transaction_id']).sum()[['disbursement_amount']]
    return df_grouped


# add a column with most recent dates
def recent_dates(final_df, df_grouped):
    # converting dates
    final_df['disbursement_date'] = pd.to_datetime(final_df['disbursement_date'], errors = 'ignore')

    # getting unique transactions
    transactions = final_df['transaction_id'].unique()

    # getting the most recent dates and putting them in a dictionary
    recent = {}
    for i in transactions:
        final_df[final_df['transaction_id'] == i]['disbursement_date'].max()
        #print(final_df[final_df['recipient_name'] == i]['disbursement_date'].max())
        recent[i] = final_df[final_df['transaction_id'] == i]['disbursement_date'].max()

    # resetting index
    df_grouped = df_grouped.reset_index()

    # adding the dates to the df
    last_dates = []
    for i in range(len(df_grouped)):
        row = df_grouped.iloc[i]
        last_dates.append(recent[row['transaction_id']])
    last_dates

    # adding the column to the df
    df_grouped['most_recent'] = last_dates
    return df_grouped


def instances(final_df, df_grouped):

    # getting the instances of each disbursement
    total = final_df.groupby(['committee_id','recipient_name', 'disbursement_description']).count()
    total = total.reset_index()

    # getting unique transactions
    transactions = final_df['transaction_id'].unique()

    counts = {}
    for i in transactions:
        final_df[final_df['transaction_id'] == i]['amendment_indicator'].count()
        counts[i] = final_df[final_df['transaction_id'] == i]['amendment_indicator'].count()
    counts

    # resetting index
    df_grouped = df_grouped.reset_index()

    # adding the counts to the df
    counting = []
    for i in range(len(df_grouped)):
        row = final_df.iloc[i]
        counting.append(counts[row['transaction_id']])
    counting

    # adding the column to the df
    df_grouped['number_of_disbursements'] = counting
    return df_grouped


# creating a column for relevant descriptions by specified keywords
def categories(df_grouped, final_df):

    # getting the descriptions
    descriptions = df_grouped['disbursement_description']
    descriptions

    # holding them in a list
    categories = []
    for i in descriptions:
        categories.append(i)
    categories

    # iterating through all the descriptions and labeling them for gategory based on keywords
    # returning a list with a label for each description
    relevant = []
    for i in categories:
        description = i.lower()
        words = description.split(' ')
        choice = []
        category = []
        for word in words:
            if word in ['media','advertising', 'tv', 'digital','radio', 'online', 'consulting', 'media.','advertising.', 'tv.', 'digital.','radio.', 'online.', 'consulting.']:
                adding = 'yes'
                choice.append(adding)
                category.append(word)
            else:
                adding = 'no'
                choice.append(adding)
        if 'yes' in choice:
            relevant.append(category[0])
        else:
            relevant.append("other")
    relevant

    # adding a column with the labels
    df_grouped['Categories'] = relevant
    df_grouped

    # Cleaning up
    df_grouped = df_grouped.drop('index', axis=1)
    df_grouped['committee_name'] = final_df['committee_name']

    return df_grouped


# Master df function: combines all df functions
def all_df_operations(disbursements):
    if disbursements != 'error':
        try:
            original_df = pd.DataFrame(disbursements)
            final_df = original_df.copy()
            df_grouped = edit_df(final_df)
            df_grouped = recent_dates(final_df, df_grouped)
            df_grouped = instances(final_df, df_grouped)
            df_grouped = categories(df_grouped, final_df)
            return df_grouped
        except:
            code = 'df_error'
            return code
    else:
        return 'error'


def df_to_csv(processed_data, final_df, party):

    for j in final_df['state']:
        if j != None:
            state = j
            break
        else:
            state = None

    for k in final_df['office']:
        if k != None:
            office = k
            break
        else:
            office = None

    for l in final_df['district']:
        if l != None:
            if l != '00':
                district = l
                break
            else:
                district = '00'
        else:
            district = None


    path = 'FEC/DISBURSEMENTS/' + str(party) + '/' + str(state) + '/' + str(office)
    folder = 'FEC/DISBURSEMENTS/' + str(party) + '/' + str(state) + '/' + str(office) + '/'
    newpath = r'FEC/DISBURSEMENTS/' + str(party) + '/' + str(state) + '/' + str(office)

    if not os.path.exists(newpath):
        os.makedirs(newpath)

    committee_bit = committee_id  # careful to have name from previous functions
    ext_bit = ".xls"
    file_name = folder + str(committee_bit) + ext_bit

    try:
        csv = processed_data.to_excel(file_name)
        return csv
    except:
        return 'df_error'

"""
SECTION FOR REMOVING WITHDRAWN CANDIDATES
"""

state_dict = {
        'AK': 'Alaska','AL': 'Alabama', 'AR': 'Arkansas', 'AS': 'American Samoa',
        'AZ': 'Arizona', 'CA': 'California', 'CO': 'Colorado', 'CT': 'Connecticut',
        'DC': 'District of Columbia', 'DE': 'Delaware', 'FL': 'Florida',
        'GA': 'Georgia', 'GU': 'Guam', 'HI': 'Hawaii', 'IA': 'Iowa',
        'ID': 'Idaho', 'IL': 'Illinois', 'IN': 'Indiana', 'KS': 'Kansas',
        'KY': 'Kentucky', 'LA': 'Louisiana', 'MA': 'Massachusetts',
        'MD': 'Maryland', 'ME': 'Maine', 'MI': 'Michigan', 'MN': 'Minnesota',
        'MO': 'Missouri', 'MP': 'Northern Mariana Islands', 'MS': 'Mississippi',
        'MT': 'Montana', 'NA': 'National', 'NC': 'North Carolina',
        'ND': 'North Dakota', 'NE': 'Nebraska', 'NH': 'New Hampshire',
        'NJ': 'New Jersey', 'NM': 'New Mexico', 'NV': 'Nevada', 'NY': 'New York',
        'OH': 'Ohio', 'OK': 'Oklahoma', 'OR': 'Oregon', 'PA': 'Pennsylvania',
        'PR': 'Puerto Rico', 'RI': 'Rhode Island', 'SC': 'South Carolina',
        'SD': 'South Dakota', 'TN': 'Tennessee', 'TX': 'Texas', 'UT': 'Utah',
        'VA': 'Virginia', 'VI': 'Virgin Islands', 'VT': 'Vermont', 'WA': 'Washington',
        'WI': 'Wisconsin', 'WV': 'West Virginia', 'WY': 'Wyoming'
        }

def get_withdrawals(STUSPS, kind):
    """
    state = str, a U.S. state abbreviation
    kind = str, 'house' or 'senate'

    returns: list of lists. Internal list = STUSPS (str), District # (int), name (str)
        example:[
                    ['CA', 15, 'Ryan Reynolds']
                    ['NY', 12, 'Anthony Weiner']
                ]

    This master function scrapes the ballotpedia page for the 2018 election
    cycle and finds candidates that have withdrawn from their races.
    """

    if STUSPS in ['AS', 'DC', 'GU', 'MP', 'NA', 'PR', 'VI']:
        return None

    state = state_dict[STUSPS]

    if kind == 'house':
        page_url = 'https://ballotpedia.org/United_States_House_of_Representatives_elections_in_' + state + ',_2018'

    if kind == 'senate':
        page_url = 'https://ballotpedia.org/United_States_Senate_election_in_' + state + ',_2018'

    page_raw = requests.get(page_url)
    souped = BeautifulSoup(page_raw.content, "html.parser")
    raw_text = souped.get_text()

    if kind == 'house':
        if 'At-Large' in raw_text: #states with one seat are handled differently
            withdrawals = single_district_states(raw_text, STUSPS)

        else:
            withdrawals = multi_district_states(raw_text, STUSPS)

    if kind == 'senate':
        withdrawals = single_district_states(raw_text, STUSPS)

    if len(withdrawals) > 0:
        return withdrawals

def single_district_states(raw_text, state):
    """
    raw_text = str, raw text from a BeautifulSoup object
    state = str, two-letter state abbreviation

    returns list of lists

    This function is a simple handoff between 'get_withdawals' and
    'process_cutdown'
    """

    text_get_to_list = raw_text.split('\n')
    stripped_text = [x for x in text_get_to_list if x != '']
    return process_cutdown(stripped_text, state)

def multi_district_states(raw_text, state):
    """
    raw_text = str, raw text from a BeautifulSoup object
    state = str, two-letter state abbreviation

    returns list of lists

    This function is a simple handoff between 'get_withdawals' and
    'process_cutdown'
    """

    withdrawals = []
    text_get_to_list = raw_text.split('\n')

    stripped_text = [x for x in text_get_to_list if x != '']
    start = stripped_text.index('District 1')
    cutdown = stripped_text[start:]
    return process_cutdown(cutdown, state)

def process_cutdown(cutdown, state,district=0):
    """
    cutdown = list of strings
    state = str, a U.S. state abbreviation
    district = int -- ignore it

    returns: list of lists. Internal list = STUSPS (str), District # (int), name (str)

    This function isolates the portion of the str we want to address and hands
    them off to helper functions
    """

    withdrawals = []
    for i in range(len(cutdown)):
        entry = cutdown[i]

        if 'District' in entry and len(entry)<8:
            district = int(entry[-2:].strip())

        elif 'candidate' in entry:
            pass

        elif 'Withdrew' in entry:
            withdrew = entry.split(']')

            if len(withdrew) > 1:
                names = process_withdrawal_row(withdrew)

                if names != None:

                    for name in names:
                        withdrawals.append([state, district, name])

            else:
                ind = i

                for k in range(1, 5):
                    row = cutdown[ind+k]

                    if ']' in row:
                        name = process_name(row)

                        if name != False:
                            withdrawals.append([state, district, name])

                    else:
                        break

    return withdrawals

def process_withdrawal_row(row):
    """
    row = list of str with at least one name in it.

    returns: list of str (if names found) or None

    This function extracts names of withdrawn candidates
    """
    names = []
    for chunk in row:
        if process_name(chunk) != False:
            names.append(process_name(chunk))
    if len(names) > 0:
        return names

def process_name(chunk):
    """
    cunk = str, a picece of text that likely contains a name from ballotpedia

    returns str (if name found) or False (if name not found)
    """
    flag = False
    if '[' in chunk:
        flag = True
        end = chunk.index('[')
        chunk = chunk[:end]
    if ':' in chunk:
        start = chunk.index(':') + 2
        chunk = chunk[start:]
    if '-' in chunk:
        end = chunk.index('-')-1
        chunk = chunk[:end]
    if ')' in chunk:
        flag = False
    if len(chunk) < 5:
        flag = False

    if flag == True:
        return chunk.strip()
    else:
        return flag

def make_withdrawal_df(kind):
    """
    kind = str, 'house' or 'senate'

    returns DataFrame of withdrawn candidates
    """
    df = pd.DataFrame()
    for key in state_dict:
        tdf = pd.DataFrame(get_withdrawals(key, kind))
        df = pd.concat([df, tdf])
    df.columns = ['state', 'district', 'name']

    lnames = []
    for entry in df['name']:
        lname = entry.split(' ')[-1].upper()
        lnames.append(lname)

    df['lname'] = lnames

    return df.sort_values('state')

def clean_main_df(main_df, withdrawal_df, kind):
    """
    main_df = DataFrame of all targeted committees
    withdrawal_df = DataFrame of candidates who have withdrawn
    kind = str, 'house' or 'senate'

    returns a version of main_df with all the withdrawn candidates removed
    """
    dupe_df = main_df[:]
    dupe_df = dupe_df.sort_values('state')

    lnames = []
    for entry in dupe_df['name']:
        lname = entry.split(',')[0].upper()
        lnames.append(lname)

    dupe_df['lname'] = lnames

    if kind == 'senate':
        dupe_df['district'] = 0

    state = 'NA'
    remove = []

    for i in range(len(withdrawal_df)):
        w_row = withdrawal_df.iloc[i]

        if w_row['state'] != state:
            state = w_row['state']
            tdf = dupe_df[dupe_df['state'] == state]

            for k in range(len(tdf)):
                t_row = tdf.iloc[k]
                print(t_row['district'],  w_row['district'], 't', t_row['lname'], 'w', w_row['lname'])

                if t_row['district'] == w_row['district'] and t_row['lname'] == w_row['lname']:
                    remove.append(t_row['candidate_id'])

    return main_df[~main_df['candidate_id'].isin(remove)]







if __name__=='__main__':


    """
    Executing functions to fetch all data
    """
    while True:
        today = datetime.now().weekday()
        now = datetime.now()

        house_withdrawals = make_withdrawal_df('house')
        sen_withdrawals = make_withdrawal_df('senate')



        print("Today is", today)

        print("Getting democrats")

        all_dem_committees = get_committees('DEM') # use function to get all committees
        final_dem_committees = committees_with_candidate_ids('DEM') # use function to limit to committees with ids
        dem_ids = get_committee_ids('DEM') # use function to get only committee ids
        all_dem_ids = get_candidate_and_committee_ids('DEM') # use function to get candidate id with its committee id

        senate_dem = senate_money('DEM') # use function to get senate data


        house_dem = house_money('DEM') # use function to get house data



        '''Executing dataframe functions'''
        dem_senate_data = senate_data_to_df(senate_dem, 'DEM')
        dem_senate_data = clean_main_df(dem_senate_data, sen_withdrawals, 'senate')

        dem_house_data = house_data_to_df(house_dem, 'DEM')
        dem_house_data = clean_main_df(dem_house_data, house_withdrawals, 'house')


        '''Putting all candidates into one df to export'''
        all_dem_candidates = pd.concat([dem_house_data, dem_senate_data])

        # rearranging columns
        all_dem_candidates_rearranged = all_dem_candidates[['name','candidate_id','state','office','district','cash_on_hand_end_period','most_recent_date','cycle','committee_id','disbursements_file_path', 'primary_date']]
        all_dem_candidates_rearranged


        '''Exporting to Excel'''
        path = 'FEC/'
        newpath = r'FEC/'

        if not os.path.exists(newpath):
            os.makedirs(newpath)

        dem_senate_data.to_excel("FEC/Dem_Senate_Candidates.xls")
        dem_house_data.to_excel("FEC/Dem_House_Candidates.xls")
        all_dem_candidates_rearranged.to_excel("FEC/All_Dem_Candidates.xls")



        # setting up counter and dicts to keep track of things
        dem_counter = 0
        problematic_disbursements = {}
        successful_disbursements = {}
        problematic_data = {} # where disbursements work but not the df conversion

        # looping through all committees; maybe put in a step for testing
        for i in range(0, len(dem_ids)): # optional step
            committee_id = dem_ids[i]
            time.sleep(8) # figure out right amount
            disbursements = sements(committee_id)
            dem_counter += 1

            if disbursements == []:
                problematic_disbursements[committee_id] = disbursements
                #print('error: no disbursements')
                print(dem_counter)

            else:
                # print(disbursements)
                original_df = pd.DataFrame(disbursements)
                final_df = original_df.copy()

                try:
                    df_grouped = edit_df(final_df, 'DEM')
                    df_grouped = recent_dates(final_df, df_grouped)
                    df_grouped = instances(final_df, df_grouped)
                    df_grouped = categories(df_grouped, final_df)
                    processed_data = df_grouped # using master df function
                    #print("got a df")


                    getting_csv = df_to_csv(processed_data, final_df, 'DEM') # saving df to csv
                    getting_csv
                    successful_disbursements[committee_id] = 'succeess'
                    #print("got csv")
                    print(dem_counter)

                except:
                    #print("df error")
                    print(dem_counter)


        print("Getting republicans")


        all_rep_committees = get_committees('REP') # use function to get all committees
        final_rep_committees = committees_with_candidate_ids('REP') # use function to limit to committees with ids
        rep_ids = get_committee_ids('REP') # use function to get only committee ids
        all_rep_ids = get_candidate_and_committee_ids('REP') # use function to get candidate id with its committee id

        senate_rep = senate_money('REP') # use function to get senate data
        house_rep = house_money('REP') # use function to get house data


        '''Executing dataframe functions'''
        rep_senate_data = senate_data_to_df(senate_rep, 'REP')
        rep_senate_data = clean_main_df(rep_senate_data, sen_withdrawals, 'senate')

        rep_house_data = house_data_to_df(house_rep, 'REP')
        rep_hosue_data = clean_main_df(rep_house_data, sen_withdrawals, 'house')


        '''Putting all candidates into one df to export'''
        all_rep_candidates = pd.concat([rep_house_data, rep_senate_data])

        # rearranging columns
        all_rep_candidates_rearranged = all_rep_candidates[['name','candidate_id','state','office','district','cash_on_hand_end_period','most_recent_date','cycle','committee_id','disbursements_file_path', 'primary_date']]
        all_rep_candidates_rearranged


        '''Exporting to Excel'''
        path = 'FEC/'
        newpath = r'FEC/'

        if not os.path.exists(newpath):
            os.makedirs(newpath)

        rep_senate_data.to_excel("FEC/Rep_Senate_Candidates.xls")
        rep_house_data.to_excel("FEC/Rep_House_Candidates.xls")
        all_rep_candidates_rearranged.to_excel("FEC/All_Rep_Candidates.xls")



        # setting up counter and dicts to keep track of things
        rep_counter = 0

        # looping through all committees; maybe put in a step for testing
        for i in range(0, len(rep_ids)): # optional step
            committee_id = rep_ids[i]
            time.sleep(5) # figure out right amount
            disbursements = sements(committee_id)
            rep_counter += 1

            if disbursements == []:
                problematic_disbursements[committee_id] = disbursements
                #print('error: no disbursements')
                print(rep_counter)

            else:
                # print(disbursements)
                original_df = pd.DataFrame(disbursements)
                final_df = original_df.copy()

                try:
                    df_grouped = edit_df(final_df, 'REP')
                    df_grouped = recent_dates(final_df, df_grouped)
                    df_grouped = instances(final_df, df_grouped)
                    df_grouped = categories(df_grouped, final_df)
                    processed_data = df_grouped # using master df function
                    #print("got a df")


                    getting_csv = df_to_csv(processed_data, final_df, 'REP') # saving df to csv
                    getting_csv
                    successful_disbursements[committee_id] = 'succeess'
                    #print("got csv")
                    print(rep_counter)

                except:
                    #print("df error")
                    print(rep_counter)






        print("ALL DONE!!!")
        print("Last update at: ", now)
        time.sleep(72000)
